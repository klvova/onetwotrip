module.exports = {
	extends: 'eslint-config-airbnb',
	parser: 'babel-eslint',
    globals: {
		fetch: true,
        document: true
    },
	rules: {
		indent: ['error', 4, { SwitchCase: 1 }],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
		'react/jsx-filename-extension': 0
	}
};
