import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import initialState from './initial-state';
import rootReducer from '../reducers/rootReducer';

const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(
        thunkMiddleware,
    ),
);

export default store;
