const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        app: [
            'webpack-dev-server/client?http://localhost:8080',
            'webpack/hot/only-dev-server',
            'react-hot-loader/patch',
            './client/app.js',
        ],
        vendor: ['react', 'redux', 'react-redux', 'react-dom', 'react-router', 'redux-thunk', 'react-hot-loader', 'reselect'],
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js'],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                'react',
                                ['es2015', { modules: false }],
                                'stage-0',
                            ],
                            plugins: [
                                'react-hot-loader/babel',
                                'transform-decorators-legacy',
                                'transform-object-rest-spread',
                            ],
                        },
                    },
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader',
                }),
            },
        ],
    },
    devServer: {
        hot: true,
        contentBase: __dirname,
        publicPath: '/',
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new ExtractTextPlugin('styles.css'),
    ],
    devtool: 'cheap-eval-source-map',
};
