import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { fetchInfo, chooseCompany } from '../../actions';
import format from 'date-fns/format'
import ruLocale from 'date-fns/locale/ru'
import './App.css';

const availableCompainesSelector = createSelector(
    state => state.info.all,
    items => items.reduce((carriers, flight) => {
        if (!(flight.carrier in carriers)) {
            carriers[flight.carrier] = [];
        }
        carriers[flight.carrier].push(flight);
        return carriers;
    }, {}),
);

const flightsByCompany = createSelector(
    state => state.info.chosen_carrier,
    availableCompainesSelector,
    (carrier, carriers) => carriers[carrier],
);

@connect(
    (state, props) => {
        const availableCarriers = availableCompainesSelector(state);
        const flightsByCarrier = (state.info.chosen_carrier !== "all")
            ? flightsByCompany(state)
            : state.info.all;
        return {
            ...props,
            available_companies: availableCarriers,
            flights_by_company: flightsByCarrier,
        };
    },
    dispatch => ({
        dispatch,
    }),
)

class App extends PureComponent {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        available_companies: PropTypes.arrayOf(PropTypes.object).isRequired,
        flights_by_company: PropTypes.arrayOf(PropTypes.object).isRequired,
    };
    constructor(props) {
        super(props);
        this.chooseCompany = this.chooseCompany.bind(this);
    }
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchInfo());
    }

    chooseCompany(company) {
        this.props.dispatch(chooseCompany(company));
    }
    render() {
        const { available_companies, flights_by_company } = this.props;
        let options = [{
            value: "all",
            label: 'Все авиакомпании',
            selected: true,
        }];
        options = options.concat(Object.keys(available_companies).map(company => ({
            value: company,
            label: company,
        })));
        return (<div className="container">
            <Select
                options={options}
                onChange={this.chooseCompany}
            />
            <div className="cards">
                {flights_by_company.map(flight =>
                    <Card flight={flight} key={flight.id} />,
                )}
            </div>

        </div>);
    }
}

class Select extends PureComponent {
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
    }

    change(e) {
        debugger;
        this.props.onChange(e.target.value);
    }

    render() {
        const {options} = this.props;
        return (
            <div>
                <select onChange={this.change}>
                    {options.map((option, key) => {
                        return <option selected={options.selected} value={option.value}>{option.label}</option>
                    })}
                </select>
            </div>
        );
    }
}


class Card extends PureComponent {
    static propTypes = {
        flight: PropTypes.object.isRequired,
    };

    componentDidMount() {}
    render() {
        const { direction: { from, to }, arrival, departure, carrier } = this.props.flight;
        return (<div className="card">
            <div className="card__header">Билет авиакомпании { carrier}</div>
            <div className="card__title">Из: {from}</div>
            <div className="card__title">В: {to}</div>
            <div className="card__meta">Вылет в: {format(departure, 'YYYY-MM-DD в HH:mm', { locale: ruLocale })}</div>
            <div className="card__meta">Прибытие в: {format(arrival, 'YYYY-MM-DD в HH:mm', { locale: ruLocale })}</div>
        </div>);
    }
}

export default App;
