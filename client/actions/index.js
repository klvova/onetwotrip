import { pathFetchInfo } from '../constants/index';
import * as ACTION_TYPES from '../constants/action_types';

const receiveInfo = json => ({
    type: ACTION_TYPES.FETCH_INFO_SUCCESS,
    flights: json.flights,
});

// eslint-disable-next-line import/prefer-default-export
export function fetchInfo() {
    return dispatch =>
        // dispatch(requestPosts(subreddit))
        fetch(pathFetchInfo)
            .then(response => response.json())
            .then(json => dispatch(receiveInfo(json)));
}

export function chooseCompany(carrier) {
    return {
        type: ACTION_TYPES.CHOOSE_COMPANY,
        carrier
    }
}