import * as ACTION_TYPES from '../constants/action_types';

export default function info(state = { all: [], chosen_carrier: 'all'}, action) {
    switch (action.type) {
        case ACTION_TYPES.FETCH_INFO_SUCCESS:
            console.log(action);
            return {
                ...state,
                all: action.flights,
            };
        case ACTION_TYPES.CHOOSE_COMPANY:
            return {
                ...state,
                chosen_carrier: action.carrier,
            }
        default:
            return state;
    }
}
