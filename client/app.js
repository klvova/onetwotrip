import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './components/App';
import store from './store/store';

function r(Component) {
    render(
        <AppContainer>
            <Provider store={store}>
                <Component />
            </Provider>
        </AppContainer>,
        document.getElementById('react-root'),
    );
}

r(App);

if (module.hot) {
    module.hot.accept('./components/App', () => {
        r(App);
    });
}
